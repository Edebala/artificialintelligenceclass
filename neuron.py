import random
import aktivacios_fuggvenyek

class neuron:
    def __init__(self,theta,W):
        self.theta = theta
        self.W = W

    def __init__(self,theta):
        self.theta = theta
        self.W = []
        for i in range(9):
            self.W.append(random.random())
    
    def threshold(self,X):
        v = 0
        for i in range(len(X)):
            v += X[i] * self.W[i]
        v = [v]
        return aktivacios_fuggvenyek.thresholdFunction(v,self.theta)[0]
