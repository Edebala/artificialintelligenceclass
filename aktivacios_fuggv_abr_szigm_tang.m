%%Feladat aktivációs függvények ábrázolására
clear all;
close all;
%%Tangens hiperbolikus függvény ábrázolása az a paraméter függvényében

%figure(1);
figure('position', [0, 0, 600, 300])
hold on;
%grid on;

szin={'. ','--','-.'}
teta=0


min_x=-6
max_x=6
tang_hip=@(s,a,teta) (1-exp(-(a*s-teta)))./(1+exp(-(a*s-teta)))
a=.5:.5:1.5;
s=min_x:0.1:max_x

for i=1:length(a)
    y=tang_hip(s,a(i), 0); %s függvényváltozó, a  változtatott paraméter
    plot(s,y,szin{i},'Linewidth',1);
  %  hold on
    legend_text{i}=strcat('a=',num2str(a(i)))
end;
 
 xlabel('Inger');
 ylabel('Válasz');
 legend(legend_text,'location', 'northwest')
 
 set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-1.1 1.1])
set(gca,'YTick',[-1.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
%text(a(i)+0.2, 0.4,strcat('a =',num2str(a(i))),'FontSize',14)
set(gca,'FontSize',18)
grid
print('-dpng','-r300','tanszig_a');
 %title('Tangens hiperbolikus függvény tanulmányozása az a paraméter függvényében');
 
 %Tangens hiperbolikus függvény ábrázolása a \theta paraméter függvényében
 
%figure(2);
figure('position', [0, 0, 600, 300])
hold on;
%grid on;
%szin=['r','g','b']
teta=0:.5:1;
for i=1:length(a)
    y=tang_hip(s,1, teta(i));   %konstans  teta valtozik
    plot(s,y,szin{i},'Linewidth',1);
    legend_text{i}=strcat('\theta=',num2str(teta(i)));
end;
 
 xlabel('Inger');
 ylabel('Válasz');
legend(legend_text,'location', 'northwest')
 %title('Tangens hiperbolikus függvény tanulmányozása a \theta paraméter függvényében');
  
 set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-1.1 1.1])
set(gca,'YTick',[-1.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
%text(teta(i)+0.2, 0.4,strcat('\theta =',num2str(teta(i))),'FontSize',14)
set(gca,'FontSize',18)
grid
print('-dpng','-r300','tanszig_theta');
 
%%Szigmoid függvény ábrázolása az a paraméter függvényében
%figure(3);
figure('position', [0, 0, 600, 300])
hold on;
%grid on;
%szin=['r','g','b']
szigm=@(s,a,teta) 1./(1+exp(-(a*s-teta)))
a=.5:.5:1.5;
s=-10:0.1:10
for i=1:length(a)
    y=szigm(s,a(i),0);   %a valtozo ; teta konstans
    plot(s,y,szin{i},'Linewidth',1);
    legend_text{i}=strcat('a=',num2str(a(i)));
end;
 
 xlabel('Inger');
 ylabel('Válasz');
legend(legend_text,'location', 'northwest')
 %title('Szigmoid függvény tanulmányozása az a paraméter függvényében');
   
 set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-0.1 1.1])
set(gca,'YTick',[-0.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
%text(a(i)+0.2, 0.4,strcat('a =',num2str(a(i))),'FontSize',14)
set(gca,'FontSize',18)
grid
print('-dpng','-r300','szig_a');
 
 %Szigmoid függvény ábrázolása az \theta paraméter függvényében
%figure(4);
figure('position', [0, 0, 600, 300])
hold on;
%grid on;
%szin=['r','g','b']
szigm=@(s,a,teta) 1./(1+exp(-(a*s-teta)))
teta=0:.5:1;
 
for i=1:length(a)
    y=szigm(s,1,teta(i));   %a konstans   \theta valtozo
    plot(s,y,szin{i},'Linewidth',1);
    legend_text{i}=strcat('\theta=',num2str(teta(i)));
end;
 
 xlabel('Inger');
 ylabel('Válasz');
legend(legend_text,'location', 'northwest')
 %title('Szigmoid függvény tanulmányozása a \theta paraméter függvényében');
   
 set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-0.1 1.1])
set(gca,'YTick',[-0.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
%text(teta(i)+0.2, 0.4,strcat('\theta =',num2str(teta(i))),'FontSize',14)
set(gca,'FontSize',18)
grid
print('-dpng','-r300','szig_theta');
