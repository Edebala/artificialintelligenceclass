import numpy as np
from math import pi
from matplotlib import pyplot as plt
import matplotlib

def thresholdFunction(X,theta):
    newX = []
    for x in X:
        if x >theta:
            newX.append(1)
        else:
            newX.append(0)
    return newX

def stepFunction(x,theta):
    newX = []
    for x in X:
        if x >theta:
            newX.append(1)
        else:
            newX.append(-1)
    return newX

def linearFunction(x,theta):
    newX = []
    for x in X:
        newX.append(x-theta)
    return newX

def saturatedLinearFunction(x,theta):
    newX = []
    for x in X:
        if x+1 < theta:
            newX.append(-1)
        else:
            if x-1 < theta:
                newX.append(x-theta)
            else:
                newX.append(1)
    return newX

def ReLU(X,theta):
    newX = []
    for x in X:
        if x >theta:
            newX.append(x-theta)
        else:
            newX.append(0)
    return newX

def LReLU(X,theta,alpha):
    newX = []
    for x in X:
        if x >theta:
            newX.append(x-theta)
        else:
            newX.append(alpha*(x-theta))
    return newX

def ELU(X,theta,alpha):
    newX = []
    for x in X:
        if x >theta:
            newX.append(x-theta)
        else:
            newX.append(alpha*(np.exp(x-theta) -1))
    return newX

def logisticFunction(X,a,theta):
    try:
        X = float(X)
        return 1/(1+np.exp(-a*(X-theta)))
    except TypeError:
        pass
    newX = []
    for x in X:
        newX.append(1/(1+np.exp(-a*(x-theta))))
    return newX

def tangentHyperbolic(X,a,theta):
    try:
        X = float(X)
        return (1-np.exp(-a*(x-theta)))/(1+np.exp(-a*(x-theta)))
    except TypeError:
        pass
    newX = []
    for x in X:
        newX.append((1-np.exp(-a*(x-theta)))/(1+np.exp(-a*(x-theta))))
    return newX

"""
X = np.arange(-2,2,0.01)
newX = thresholdFunction(X,1)
plt.plot(X,newX)
newX = stepFunction(X,1)
plt.plot(X,newX)
newX = linearFunction(X,1)
ax1.plot(X,newX)
"""

