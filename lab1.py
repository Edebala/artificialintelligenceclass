from aktivacios_fuggvenyek import *
import random
from neuron import neuron

def I1feladat():
    fig, (ax1,ax2) = plt.subplots(2)
    X = np.arange(-6,6,0.01)
    newX = logisticFunction(X,0.5,1)
    ax1.plot(X,newX)
    newX = logisticFunction(X,0.75,1)
    ax1.plot(X,newX)
    newX = logisticFunction(X,1,1)
    ax1.plot(X,newX)
    newX = logisticFunction(X,1.25,1)
    ax1.plot(X,newX)
    newX = logisticFunction(X,1.5,1)
    ax1.plot(X,newX)

    newX = tangentHyperbolic(X,0.5,1)
    ax2.plot(X,newX)
    newX = tangentHyperbolic(X,0.75,1)
    ax2.plot(X,newX)
    newX = tangentHyperbolic(X,1,1)
    ax2.plot(X,newX)
    newX = tangentHyperbolic(X,1.25,1)
    ax2.plot(X,newX)
    newX = tangentHyperbolic(X,1.5,1)
    ax2.plot(X,newX)
    ax1.grid(True)
    ax2.grid(True)
    plt.show()

def I2feladat():
    fig, (ax1,ax2) = plt.subplots(2)
    X = np.arange(-6,6,0.01)
    newX = logisticFunction(X,1,0)
    ax1.plot(X,newX)
    newX = logisticFunction(X,1,0.5)
    ax1.plot(X,newX)
    newX = logisticFunction(X,1,1)
    ax1.plot(X,newX)
    newX = logisticFunction(X,1,1.5)
    ax1.plot(X,newX)
    newX = logisticFunction(X,1,2)
    ax1.plot(X,newX)

    newX = tangentHyperbolic(X,1,0)
    ax2.plot(X,newX)
    newX = tangentHyperbolic(X,1,0.5)
    ax2.plot(X,newX)
    newX = tangentHyperbolic(X,1,1)
    ax2.plot(X,newX)
    newX = tangentHyperbolic(X,1,1.5)
    ax2.plot(X,newX)
    newX = tangentHyperbolic(X,1,2)
    ax2.plot(X,newX)
    ax1.grid(True)
    ax2.grid(True)
    plt.show()

def I3feladat():
    X = np.arange(-3,3,0.01)
    Y = ELU(X,1,0.5)
    plt.plot(X,Y)
    plt.grid(True)
    plt.show()

def I4feladat():
    fig, (ax1,ax2) = plt.subplots(2)

    X = np.arange(-6,6,0.01)

    Y1 = logisticFunction(X,1,0)
    for i in range(len(Y1)):
        Y1[i] = Y1[i]*(1-Y1[i])
    ax1.plot(X,Y1)
    ax1.grid(True)

    Y2 = []
    for i in range(len(X)):
        Y2.append(2/(np.exp(-(X[i]*X[i])) + np.exp(X[i]*X[i])))
    ax2.plot(X,Y2)
    ax2.grid(True)
    plt.show()


def II1feladat():
    N = neuron(random.randint(2,8))
    print("Theta = ",N.theta)
    print("W = ", N.W)

def II2feladat():
    N = [] 
    for i in range(5):
        N.append(neuron(5))

    T = [1,1,1,0,1,0,0,1,0]
    H = [1,0,1,1,1,1,1,0,1]
    O = [1,1,1,1,0,1,1,1,1]

    for n in N:
        print(n.W)
        print(n.threshold(T))
        print(n.threshold(H))
        print(n.threshold(O))

def all():
    I1feladat()
    I2feladat()
    I3feladat()
    I4feladat()
    II1feladat()
    II2feladat()
