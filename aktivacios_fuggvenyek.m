close all
%k�sz�bf�ggv�ny
F_kuszob=@(x, theta) (x<=theta).*0+(x>=theta).*1;

min_x=-6;
max_x=6;
figure('position', [0, 0, 600, 300])
x=min_x:0.01:max_x;
theta=0;
y=F_kuszob(x,theta);
plot(x,y,'Linewidth',2)
hold on
theta=1;
y=F_kuszob(x,theta);
plot(x,y,'Linewidth',2)
xlabel('Inger')
ylabel('V�lasz')

set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-0.1 1.1])
set(gca,'YTick',[-0.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
text(theta+0.2, 0.4,strcat('\theta =',num2str(theta)))
set(gca,'FontSize',18)
grid
print('-dpng','-r300','kuszobfuggveny');


%l�pcs�f�ggv�ny
F_lepcso=@(x, theta) (x<=theta).*-1+(x>=theta).*1
figure('position', [0, 0, 600, 300])
theta=0;
y=F_lepcso(x,theta);
% plot(x,y,'Linewidth',2)
% hold on
theta=1;
y=F_lepcso(x,theta);
plot(x,y,'Linewidth',2)
xlabel('Inger')
ylabel('V�lasz')

set(gca,'XLim',[min_x max_x])
set(gca,'XTick',min_x: 2: max_x)
set(gca,'YLim',[-1.1 1.1])
set(gca,'YTick',-1.2: 0.2: 1.2)
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
text(theta+0.2, 0.4,strcat('\theta =',num2str(theta)))
set(gca,'FontSize',18)
grid on;
print('-dpng','-r300','lepcsofuggveny');




%linearis
F_lin=@(x, theta) x-theta
figure('position', [0, 0, 600, 300])
theta=0;
y=F_lin(x,theta)
% plot(x,y,'Linewidth',2)
% hold on
theta=1;
y=F_lin(x,theta)
plot(x,y,'Linewidth',2)
xlabel('Inger')
ylabel('V�lasz')

set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-1.1 1.1])
set(gca,'YTick',[-1.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
text(theta+0.2, 0.4,strcat('\theta =',num2str(theta)))
set(gca,'FontSize',18)
grid
print('-dpng','-r300','linearis');


%teliteses linearis
F_tellin=@(x, theta) (x<(-1+theta)).*-1 + and((x>=(-1+theta)),x<(1+theta)).*(x-theta)+(x>=(1+theta)).*1
figure('position', [0, 0, 600, 300])
theta=0;
y=F_tellin(x,theta)
% plot(x,y,'Linewidth',2)
% hold on
theta=1;
y=F_tellin(x,theta)
plot(x,y,'Linewidth',2)
xlabel('Inger')
ylabel('V�lasz')

set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-1.1 1.1])
set(gca,'YTick',[-1.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
text(theta+0.2, 0.4,strcat('\theta =',num2str(theta)))
set(gca,'FontSize',18)
grid
print('-dpng','-r300','teliteses_linearis');




%RELU
F_RELU=@(x, theta) (x<theta).*0+(x>=theta).*(x-theta)
figure('position', [0, 0, 600, 300])
theta=0;
y=F_RELU(x,theta)
% plot(x,y,'Linewidth',2)
% hold on
theta=1;
y=F_RELU(x,theta)
plot(x,y,'Linewidth',2)
xlabel('Inger')
ylabel('V�lasz')

set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-0.1 1.1])
set(gca,'YTick',[-0.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
text(theta+0.2, 0.4,strcat('\theta =',num2str(theta)))
set(gca,'FontSize',18)
grid
print('-dpng','-r300','RELU');



%LRELU
F_LRELU=@(x, theta,alpha) (x<theta).*(x-theta)*alpha+(x>=theta).*(x-theta)
figure('position', [0, 0, 600, 300])
theta=0;
alpha=0.05
y=F_LRELU(x,theta,alpha)
% plot(x,y,'Linewidth',2)
% hold on
theta=1;
y=F_LRELU(x,theta,alpha)
plot(x,y,'Linewidth',2)
xlabel('Inger')
ylabel('V�lasz')

set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-1.1 1.1])
set(gca,'YTick',[-1.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
text(theta+0.2, 0.4,strcat('\theta =',num2str(theta)))
set(gca,'FontSize',18)
grid
print('-dpng','-r300','LRELU');

%ELU
F_ELU=@(x, theta,alpha) (x<theta).*((exp(x-theta)-1)*alpha)+(x>=theta).*(x-theta)
figure('position', [0, 0, 600, 300])
theta=0;
alpha=0.2
y=F_ELU(x,theta,alpha)
% plot(x,y,'Linewidth',2)
% hold on
theta=1;
y=F_ELU(x,theta,alpha)
plot(x,y,'Linewidth',2)
xlabel('Inger')
ylabel('V�lasz')

set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-1.1 1.1])
set(gca,'YTick',[-1.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
text(theta+0.2, 0.4,strcat('\theta =',num2str(theta)))
set(gca,'FontSize',18)
grid
print('-dpng','-r300','ELU');


%GAUSS
c=1, sigma=1
F_GAUSS=@(x, c,sigma) exp(-((x-c).^2)/(2*sigma*sigma))
figure('position', [0, 0, 600, 300])
theta=0;
alpha=0.2
y=F_GAUSS(x,c,sigma)
% plot(x,y,'Linewidth',2)
% hold on
theta=1;
y=F_GAUSS(x,c,sigma)
plot(x,y,'Linewidth',2)
xlabel('Inger')
ylabel('V�lasz')

set(gca,'XLim',[min_x max_x])
set(gca,'XTick',[min_x: 2: max_x])
set(gca,'YLim',[-0.1 1.1])
set(gca,'YTick',[-0.2: 0.2: 1.2])
line([min_x,max_x],[0, 0],'Linewidth',1)
line([0 0],[-1.1,1.1],'Linewidth',1)
text(theta+0.2, 0.4,strcat('c =',num2str(c)))
set(gca,'FontSize',18)
grid
print('-dpng','-r300','GAUSS');


figure
%k�sz�bf�ggv�ny
F_kuszob=@(x, theta) (x<theta).*0+(x>=theta).*(x-theta)

x=-10:0.1:10
theta=0;
y=F_kuszob(x,theta)
plot(x,y)
hold on
theta=1;
y=F_kuszob(x,theta)
plot(x,y)
